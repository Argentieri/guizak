package org.pietro.guizak.guizak;

import java.awt.Component;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.event.EventListenerList;

import org.apache.commons.lang3.StringUtils;
import org.pietro.guizak.common.ConnectionEvent;
import org.pietro.guizak.common.ConnectionListener;
import org.zmq.ZeroMQ;

public class GuiController  {
private  boolean fast;
private  boolean complete;
private  boolean total;
private boolean confirmOp;
private String ipServer;
private String password;
static GuiController instance;
private	static Properties prop;
private String port="5555";
public static String CMD_STOP="P";
public static String CMD_START="*S";
public static String CMD_PROFILE="*****Y*";
public static String CMD_EMPTYSTAR="******";
public static String CMD_CONNECT="CONNECT";
public static String CMD_CLOSE="CLOSING";
public static String CMD_FAST="FAST";
public static String CMD_COMPLETE="COMP";
public static String CMD_TOTAL="TOTL";
public  String iPClient="198168001028";
public  String url;
//public static String IP_RECIV="198161017001";
private boolean isConnected=false; 
protected EventListenerList listenerList = new EventListenerList();
private static CtrlPanel main;
private Logger logger;


public void closeNodeD() {
main.closeNodeD();
}


public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getiPClient() {
	return iPClient;
}
public void setiPClient(String iPsend) {
	this.iPClient = iPsend;
}

public String getIpServer() {
	return ipServer;
}
public void setIpServer(String ipServer) {
	this.ipServer = ipServer;
}
public boolean isConnected() {
	return isConnected;
}
public void setConnected(boolean isConnected) {
	this.isConnected = isConnected;
}
private GuiController() {
	 logger = LoggerFactory.getLogger(GuiController.class);
	 String ipSend="";
	 try(final DatagramSocket socket = new DatagramSocket()){
		  socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
		  String[] ipSplit = socket.getLocalAddress().getHostAddress().split("\\.");
		  for (int i=0 ;i<ipSplit.length;i++) {
			  ipSplit[i]=StringUtils.leftPad(ipSplit[i], 3,"0");
			  ipSend=ipSend+ipSplit[i];		
		  }
		  this.setiPClient(ipSend);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
 }
 public static GuiController getInstance(CtrlPanel mainP) {
	if (instance==null) {
		 instance= new GuiController();
		  prop = new Properties();
		  main =mainP;
	}
	return instance;
			 
 }
public boolean isFast() {
	return fast;
}
public void setFast(boolean fast) {
	this.setFlags(fast, complete, total);
	this.fast = fast;
	if(main!=null)
		main.setFlags(fast, complete, total);
}
public boolean isComplete() {
	
	return complete;
}
public void setComplete(boolean complete) {
	this.setFlags(fast, complete, total);
	this.complete = complete;
	if(main!=null)
	main.setFlags(fast, complete, total);

}
public boolean isTotal() {
	return total;
}
public void setTotal(boolean total) {
	this.total = total;
	if (main!=null)
	main.setFlags(fast, complete, total);
	this.setFlags(fast, complete, total);

}
public void setFlags(boolean fast, boolean comp, boolean tot) {

	this.fast=fast;
	this.complete=comp;
	this.total=tot;
}

public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}


public String getPort() {
	return port;
}
public void setPort(String port) {
	this.port = port;
}
public boolean isConfirmOp() {
	return confirmOp;
}
	public void setConfirmOp(boolean confirmOp) {
		this.confirmOp = confirmOp;
	}
	public void saveConfiguration() throws IOException, URISyntaxException {
		FileOutputStream fo=new FileOutputStream(new File("./config.properties"));
		
		if(this.ipServer!=null)
		this.prop.setProperty("IP",this.ipServer); 
		if(this.password!=null)
		prop.setProperty("password", this.password); 
		prop.setProperty("fast",new Boolean(this.fast).toString());
		prop.setProperty("complete", new Boolean(this.complete).toString());
		prop.setProperty("total", new Boolean(total).toString());
		prop.setProperty("Confirm", new Boolean(confirmOp).toString());
		prop.store(fo, null);
		fo.close();
	}
	public  boolean connect(Component parent,String password,String ipAdd,String ipSend) {
		try {
			boolean ok= ZeroMQ.instance().connect(password, ipAdd);
			fireConnectedEvent(new ConnectionEvent(parent,true));
			isConnected=ok;
			/*if (this.total||this.complete) {
				main.disableStart();
			}*/
			return ok;
			
		} catch (ConnectException e) {
			JOptionPane.showMessageDialog(parent,e.getMessage() , "Error", JOptionPane.ERROR_MESSAGE);
			isConnected=false;
			return false;
		}	   
	}
	public  boolean connect() {
		try {
			boolean ok= ZeroMQ.instance().connect(password, this.ipServer);
			//fireConnectedEvent(new ConnectionEvent(null,true));
			isConnected=ok;
			/*if (this.total||this.complete) {
				main.disableStart();
			}*/
			return ok;
			
		} catch (ConnectException e) {
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null,e.getMessage() , "Error", JOptionPane.ERROR_MESSAGE);
			isConnected=false;
			return false;
		}
		   
	}
	public  boolean disconnect() {
		
			ZeroMQ.instance().closeConnection();
			fireDisconnectedEvent(new ConnectionEvent(this,true));
			
			return true;
	}
	public void setState(String msg) {
		
	}
	public void close() {
		ZeroMQ.instance().closeConnection();
	}
	public void sendCommand(String pwd,String cmd) {
		StringBuilder msg=new StringBuilder();
		if (isFast())
		msg=msg.append(ipServer).append(pwd).append(iPClient).append("*******").append(CMD_FAST).append(cmd);
		else if(isComplete())
			msg=msg.append(ipServer).append(pwd).append(iPClient).append("*******").append(CMD_COMPLETE).append("*S");
		else if(isTotal())
		msg=msg.append(ipServer).append(pwd).append(iPClient).append("*******").append(CMD_TOTAL).append(cmd);
		else 
		msg=msg.append(ipServer).append(pwd).append(iPClient).append("*****").append(cmd);

		logger.debug("Sending command:"+msg.toString());
		ZeroMQ.instance().sendCmd(msg.toString());
	}
	public void sendStop() {
		StringBuilder msg=new StringBuilder();
		
		msg=msg.append(this.ipServer).append(this.password).append(this.iPClient).append("************").append(CMD_STOP);
		
		logger.debug("Sending command:"+msg.toString());
		ZeroMQ.instance().sendCmd(msg.toString());
	}
	public void sendClose(String pwd) {
		StringBuilder msg=new StringBuilder();
		 
		msg=msg.append(this.ipServer).append(this.password).append(this.iPClient).append(CMD_CLOSE).append(CMD_EMPTYSTAR);

		System.out.println("Sending command:"+msg.toString());
		ZeroMQ.instance().sendCmd(msg.toString());	
	}
	public String sendChangeProfile(String pwd) {
		StringBuilder msg=new StringBuilder();
		 
		msg=msg.append(this.ipServer).append(this.password).append(this.iPClient).append(CMD_EMPTYSTAR).append(CMD_PROFILE);

		System.out.println("Sending command:"+msg.toString());
		return ZeroMQ.instance().sendCmd(msg.toString());
		
	}

	
	public void addConnectionListener(ConnectionListener listener) {
	    listenerList.add(ConnectionListener.class, listener);
	  }
	  public void removeConnectionListener(ConnectionListener listener) {
	    listenerList.remove(ConnectionListener.class, listener);
	  }
	  void fireConnectedEvent(ConnectionEvent evt) {
	    Object[] listeners = listenerList.getListenerList();
	    for (int i = 0; i < listeners.length; i ++) {
	      if (listeners[i] == ConnectionListener.class) {
	        ((ConnectionListener) listeners[i+1]).connected(evt);
	      }
	    }
	  }
	  void fireDisconnectedEvent(ConnectionEvent evt) {
		    Object[] listeners = listenerList.getListenerList();
		    for (int i = 0; i < listeners.length; i++) {
		      if (listeners[i] == ConnectionListener.class) {
		        ((ConnectionListener) listeners[i+1]).disconnected(evt);
		      }
		    }
		  }
	  void closeGUI() {
		  main.dispose();
	  }

	}
