package org.pietro.guizak.guizak;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.pietro.guizak.common.IPKeyListener;
import org.pietro.guizak.common.IPTextFieldVerifier;
import org.pietro.guizak.common.PwdVerifier;

public class NodeDialog extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtIP;
	private JTextField txtIP0;
	private JPasswordField txtPwd;
	private final JButton btnConnect = new JButton("Connect");
	private JButton btnStopCon;
	private GuiController ctr;
	private JTextField txtIP1;
	private JTextField txtIP2;
	private JTextField txtIP3;
	private JLabel lblPort;
	private JTextField textPort;

	NodeDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, false);
		btnConnect.addActionListener(this);
		ctr = GuiController.getInstance((CtrlPanel) this.getParent());
		btnConnect.addActionListener(this);

		Dimension parentSize = this.getParent().getSize();
		Point p = this.getParent().getLocation();
		setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		setMinimumSize(new Dimension(180, 150));
		// setPreferredSize(new Dimension(500, 400));
		this.setResizable(true);
		initialize();
	}

	/**
	 * Create the dialog.
	 */
	public void initialize() {
		setAlwaysOnTop(true);
		// setUndecorated(true);
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setModal(true);
		getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);

		// setBounds(100, 100, 450, 300);
		// getContentPane().setLayout(new BorderLayout());
		contentPanel.setSize(new Dimension(150, 300));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 87, 90, 116, 129, 99, 40, 0, 0 };
		gbl_contentPanel.rowHeights = new int[] { 1, 0, 104, 0, 0, 0, 0 };
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE, 0.0, 0.0 };
		contentPanel.setLayout(gbl_contentPanel);
		// contentPanel.setLayout(new BorderLayout());
		{

			{
				contentPanel.setSize(this.getSize());
			}
			JLabel lblNewLabel = new JLabel("IP");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 0;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);

			txtIP = new JTextField();
			txtIP0 = new JTextField();
			txtIP0.setColumns(3);
			txtIP0.setHorizontalAlignment(SwingConstants.LEFT);
			txtIP0.setMinimumSize(new Dimension(40, 32));

			txtIP0.addKeyListener(new IPKeyListener());

			getContentPane().add(contentPanel);
			txtIP0.setInputVerifier(new IPTextFieldVerifier());
			txtIP0.setToolTipText("IP Address");
			GridBagConstraints gbc_txtIP0 = new GridBagConstraints();
			gbc_txtIP0.insets = new Insets(0, 0, 5, 5);
			gbc_txtIP0.anchor = GridBagConstraints.SOUTHWEST;
			gbc_txtIP0.gridx = 1;
			gbc_txtIP0.gridy = 0;
			contentPanel.add(txtIP0, gbc_txtIP0);
			
			txtIP0.addFocusListener(new FocusListener() {

				public void focusLost(FocusEvent e) {
					ctr.setIpServer(txtIP.getText());
				}

				public void focusGained(FocusEvent e) {
					// TODO Auto-generated method stub

				}
			});
			{
				txtIP1 = new JTextField();
				
				txtIP1.setHorizontalAlignment(SwingConstants.LEFT);
				txtIP1.setColumns(3);

				txtIP1.addKeyListener(new IPKeyListener());

				txtIP1.setInputVerifier(new IPTextFieldVerifier());
				txtIP1.setToolTipText("IP Address");
				txtIP1.setMinimumSize(new Dimension(40, 32));
				txtIP1.setText((String) null);
				GridBagConstraints gbc_txtIP1 = new GridBagConstraints();
				gbc_txtIP1.anchor = GridBagConstraints.WEST;
				gbc_txtIP1.insets = new Insets(0, 60, 5, 5);
				gbc_txtIP1.gridx = 1;
				gbc_txtIP1.gridy = 0;
				contentPanel.add(txtIP1, gbc_txtIP1);
			}
			{
				txtIP2 = new JFormattedTextField("");
				
				txtIP2.setColumns(3);
				txtIP2.setMinimumSize(new Dimension(40, 32));
				txtIP2.setText("");
				txtIP2.addKeyListener(new IPKeyListener());

				txtIP2.setInputVerifier(new IPTextFieldVerifier());
				txtIP2.setToolTipText("IP Address");
				txtIP2.setText((String) null);
				GridBagConstraints gbc_txtIP2 = new GridBagConstraints();
				gbc_txtIP2.anchor = GridBagConstraints.WEST;
				gbc_txtIP2.insets = new Insets(0, 0, 5, 5);
				gbc_txtIP2.gridx = 2;
				gbc_txtIP2.gridy = 0;
				contentPanel.add(txtIP2, gbc_txtIP2);
			}
			{
				txtIP3 = new JFormattedTextField();
				
				txtIP3.setColumns(3);
				txtIP3.setMinimumSize(new Dimension(40, 32));

				txtIP3.addKeyListener(new IPKeyListener());

				txtIP3.setInputVerifier(new IPTextFieldVerifier());

				txtIP3.setToolTipText("IP Address");
				txtIP3.setText((String) null);
				GridBagConstraints gbc_txtIP3 = new GridBagConstraints();
				gbc_txtIP3.anchor = GridBagConstraints.WEST;
				gbc_txtIP3.insets = new Insets(0, 60, 5, 5);
				gbc_txtIP3.gridx = 2;
				gbc_txtIP3.gridy = 0;
				contentPanel.add(txtIP3, gbc_txtIP3);
				if (ctr.getUrl()!=null&&!ctr.getUrl().isEmpty()){
		        	  String ips[]=ctr.getUrl().substring(6, ctr.getUrl().lastIndexOf(":")).split("\\.");
		              txtIP0.setText(StringUtils.leftPad(ips[0],3,'0'));
		              txtIP1.setText(StringUtils.leftPad(ips[1],3,'0'));
		              txtIP2.setText(StringUtils.leftPad(ips[2],3,'0'));
		              txtIP3.setText(StringUtils.leftPad(ips[3],3,'0'));
				}
			}
			{
				{
					lblPort = new JLabel("Port");
					GridBagConstraints gbc_lblPort = new GridBagConstraints();
					gbc_lblPort.insets = new Insets(0, 0, 5, 5);
					gbc_lblPort.gridx = 0;
					gbc_lblPort.gridy = 1;
					contentPanel.add(lblPort, gbc_lblPort);
				}
			}
			{
				textPort = new JTextField();
				textPort.setColumns(10);
				GridBagConstraints gbc_textPort = new GridBagConstraints();
				gbc_textPort.insets = new Insets(0, 0, 5, 5);
				gbc_textPort.fill = GridBagConstraints.HORIZONTAL;
				gbc_textPort.gridx = 1;
				gbc_textPort.gridy = 1;
				contentPanel.add(textPort, gbc_textPort);
			}
			if (ctr.getIpServer() != null && !ctr.getIpServer().isEmpty()) {
				String ips[] = ctr.getIpServer().split("\\.");
				if (ips.length > 1) {
					txtIP0.setText(StringUtils.leftPad(ips[0],3,'0'));
					txtIP1.setText(StringUtils.leftPad(ips[1],3,'0'));
					txtIP2.setText(StringUtils.leftPad(ips[2],3,'0'));
					txtIP3.setText(StringUtils.leftPad(ips[3],3,'0'));
				}
			}
			if (ctr.getPort() != null) {
				this.textPort.setText(ctr.getPort());
			}
			{
				JLabel lblNewLabel_1 = new JLabel("password");
				lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
				GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
				gbc_lblNewLabel_1.fill = GridBagConstraints.VERTICAL;
				gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_1.gridx = 0;
				gbc_lblNewLabel_1.gridy = 5;
				contentPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
			}
			{
				txtPwd = new JPasswordField();

				txtPwd.addFocusListener(new FocusListener() {

					public void focusLost(FocusEvent e) {
						ctr.setPassword(new String(txtPwd.getPassword()));
					}

					public void focusGained(FocusEvent e) {
						// TODO Auto-generated method stub

					}
				});
				txtPwd.setInputVerifier(new PwdVerifier());
				GridBagConstraints gbc_textField_1 = new GridBagConstraints();
				gbc_textField_1.insets = new Insets(0, 0, 5, 5);
				gbc_textField_1.fill = GridBagConstraints.BOTH;
				gbc_textField_1.gridx = 1;
				gbc_textField_1.gridy = 5;
				contentPanel.add(txtPwd, gbc_textField_1);
				txtPwd.setColumns(10);
				txtPwd.setEchoChar('*');
			}
			txtPwd.setText(ctr.getPassword());
			{
				GridBagConstraints gbc_btnConnect = new GridBagConstraints();
				btnConnect.addActionListener(this);
				gbc_btnConnect.insets = new Insets(0, 0, 0, 5);
				gbc_btnConnect.anchor = GridBagConstraints.WEST;
				gbc_btnConnect.fill = GridBagConstraints.VERTICAL;
				gbc_btnConnect.gridx = 3;
				gbc_btnConnect.gridy = 6;
				contentPanel.add(btnConnect, gbc_btnConnect);
			}

			btnStopCon = new JButton("Stop connection");
			btnStopCon.addActionListener(this);
			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
			gbc_btnNewButton.gridx = 4;
			gbc_btnNewButton.gridy = 6;
			contentPanel.add(btnStopCon, gbc_btnNewButton);

			{

			}
		}
		pack();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnConnect) {
			try {
				String ip = StringUtils.leftPad(txtIP0.getText(),3,'0') + StringUtils.leftPad(txtIP1.getText(),3,'0') + StringUtils.leftPad(txtIP2.getText(),3,'0') + StringUtils.leftPad(txtIP3.getText(),3,'0');
				ctr.setIpServer(ip); 
				
				String url = txtIP0.getText() +"."+ txtIP1.getText() +"."+ txtIP2.getText() +"."+ txtIP3.getText()+":";
				if (this.textPort.getText() != null && !this.textPort.getText().isEmpty())
					ctr.setPort(textPort.getText());
				else
					ctr.setPort("5555");
				url="tcp://"+"localhost:"+ctr.getPort();
				ctr.setUrl(url);
				if (this.txtPwd.getPassword() != null)
				ctr.setPassword(new String(txtPwd.getPassword()));
				String  ipSend="";
				ctr.saveConfiguration();
				
			//	ipSend = "198168001002";// Inet4Address.getLocalHost().getHostAddress();
				ctr.connect(this,new String(txtPwd.getPassword()), ip, ipSend);
				if (ctr.isConnected()) {

				}

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} else if (e.getSource() == btnStopCon) {
			ctr.disconnect();

			this.dispose();

		}

	}
}
