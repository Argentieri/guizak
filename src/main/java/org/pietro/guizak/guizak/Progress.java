package org.pietro.guizak.guizak;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Progress extends JFrame {

	JProgressBar current;
	JTextArea out;
	JButton find;
	Thread runner;
	int num = 0;

	public Progress(int max) {
		super("Progress");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = new JPanel();
		pane.setLayout(new FlowLayout());
		current = new JProgressBar(0, max);
		current.setValue(0);
		current.setStringPainted(true);
		pane.add(current);
		setContentPane(pane);
	}

	public void setVal(int v) {
		current.setValue(v);
	}

	public void iterate() {
		while (num < 2000) {
			current.setValue(num);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			num += 95;
		}
	}

	
}