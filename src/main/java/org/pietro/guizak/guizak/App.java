package org.pietro.guizak.guizak;
import javax.swing.SwingUtilities;

import org.pietro.guizak.guizak.CtrlPanel;
/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[] args)
	  {
	    SwingUtilities.invokeLater(new Runnable(){
	      public void run(){
	    		CtrlPanel c=new  CtrlPanel();
	            c.setVisible(true);
	 
	      }
	    });
	  }
    
}
