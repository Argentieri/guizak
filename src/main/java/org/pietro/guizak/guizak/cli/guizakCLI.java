package org.pietro.guizak.guizak.cli;

import java.util.Scanner;

import org.pietro.guizak.guizak.GuiController;

public class guizakCLI {
	public static void main(String[] args) {
		GuiController ctr=GuiController.getInstance(null);
		ctr.setIpServer("192168001003");
		ctr.setFast(true);
		ctr.setPassword("Xy82Nd1C");
		ctr.connect(); 
		System.out.println("Enter one of the following commands:");
		System.out.println("E - stop");
		System.out.println("S - start");
		System.out.println("C - close");
		System.out.println("M - complete");
		System.out.println("F - fast");
		System.out.println("T - total");
		System.out.println("X -close app");
		Scanner scanchoice = new Scanner(System.in);
		System.out.println();
		//System.out.println("Enter \"E:Stop\", \"C:Close\" or \"S:start or \"T:Total \"F:fast \"M:Complete");
		String choiceentry = "";

		while (!choiceentry.equalsIgnoreCase("X")) {
			 choiceentry = scanchoice.next();

		    if(choiceentry.equals("S")) {
		    	ctr.sendCommand(ctr.getPassword(), GuiController.CMD_START);
		    }
		    else if(choiceentry.equals("C")) {
		        ctr.sendClose(ctr.getPassword());
		    }
		    else if(choiceentry.equals("E")) {
		        ctr.sendStop();
		    }
		    else if(choiceentry.equals("M")) {
		    	
		    	ctr.setComplete(true);
		        ctr.sendCommand(ctr.getPassword(),GuiController.CMD_COMPLETE);
		    }
		    else if(choiceentry.equals("F")) {
		        ctr.sendCommand(ctr.getPassword(), GuiController.CMD_FAST);
		    }
		    else if(choiceentry.equals("T")) {
		    	ctr.sendCommand(ctr.getPassword(), GuiController.CMD_TOTAL);
		    }
		    else {
		    	
		    }
		   
		 }
		 System.exit(0);
		}  
	
	
	}
	
		