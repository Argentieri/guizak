package org.pietro.guizak.guizak;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;

import org.pietro.guizak.common.ConnectionEvent;
import org.pietro.guizak.common.ConnectionListener;
import org.zmq.ZeroMQ;

public class CtrlPanel extends JFrame implements ActionListener, ItemListener, ConnectionListener, WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JMenuItem exit;
	private JMenuItem con;
	private JMenuItem how;
	private JMenuItem info;
	private JPanel panel;
	private JPanel statusPanel;
	private JMenu file;
	private JMenu node;
	private JRadioButton rdbtnCompleteRadio;
	

	private JRadioButton rdbtnFastradio;
	private JMenu report;
	private JRadioButton rdbtnTotalRadioButton;
	private JMenuItem acq;
	private JButton strBtn;
	private JButton stopBtn;
	private GuiController ctr;
	private JLabel imagelabel;
	private JProgressBar bar;
	private NodeDialog nodeD;
	private Object l;
	private Object oldSource;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public CtrlPanel() {
		super();
		initialize();
		setTitle("ZAK Control GUI");
		ctr.addConnectionListener(this);
	}

	public void statusPanel(String icon) {
		statusPanel = new JPanel();
		addWindowListener(this);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		this.add(statusPanel, BorderLayout.SOUTH);
		statusPanel.setPreferredSize(new Dimension(this.getWidth(), 16));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		JLabel statusLabel = new JLabel("Connection");
		ImageIcon image = new ImageIcon("./" + icon);
		imagelabel = new JLabel(image);
		statusPanel.add(statusLabel);
		statusPanel.add(imagelabel);
		bar = createProgress(0);
		statusPanel.add(bar);
	}

	public void updStatusPanelToOK() {

		ImageIcon image = new ImageIcon("./" + "gball.jpeg");
		imagelabel.setIcon(image);
	}

	public void updStatusPanelToStop() {
		ImageIcon image = new ImageIcon("./" + "redball.jpeg");
		imagelabel.setIcon(image);
	}

	public void increaseBar(int v) {
		bar.setValue(v);
	}
	public JRadioButton getRdbtnCompleteRadio() {
		return rdbtnCompleteRadio;
	}

	public void setRdbtnCompleteRadio(JRadioButton rdbtnCompleteRadio) {
		this.rdbtnCompleteRadio = rdbtnCompleteRadio;
	}

	public JRadioButton getRdbtnFastradio() {
		return rdbtnFastradio;
	}

	public void setRdbtnFastradio(JRadioButton rdbtnFastradio) {
		this.rdbtnFastradio = rdbtnFastradio;
	}

	public JRadioButton getRdbtnTotalRadioButton() {
		return rdbtnTotalRadioButton;
	}

	public void setRdbtnTotalRadioButton(JRadioButton rdbtnTotalRadioButton) {
		this.rdbtnTotalRadioButton = rdbtnTotalRadioButton;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		statusPanel("redball.jpeg");

		ctr = GuiController.getInstance(this);
		this.setSize(300, 300);
		JMenuBar mb = new JMenuBar();

		file = new JMenu("File");
		con = new JMenuItem("Connect to node");
		con.addActionListener(this);
		this.exit = new JMenuItem("Exit");
		exit.addActionListener(this);
		file.add(con);
		file.add(exit);
		mb.add(file);
		node = new JMenu("Node");
		mb.add(node);
		report = new JMenu("Report");
		JMenuItem mr = new JMenuItem("Manage Report");
		JMenuItem rr = new JMenuItem("Read Report");
		report.add(mr);
		report.add(rr);
		mr.setEnabled(false);
		rr.setEnabled(false);
		mb.add(report);
		JMenu profile = new JMenu("Profile");
		acq = new JMenuItem("acquisition");
		acq.addActionListener(this);
		profile.add(acq);
		mb.add(profile);
		JMenu help = new JMenu("Help");
		info = new JMenuItem("Infos");
		info.addActionListener(this);
		how = new JMenuItem("how it works");
		help.add(info);
		help.add(how);
		mb.add(help);
		panel = new JPanel();

		this.setBounds(100, 100, 450, 300);
		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gridBagLayout);
		ButtonGroup group = new ButtonGroup();
		rdbtnFastradio = new JRadioButton("fast");
		rdbtnFastradio.addItemListener(this);
		GridBagConstraints constrfast = new GridBagConstraints();
		constrfast.insets = new Insets(10, 50, 5, 5);
		constrfast.gridx = 0;
		constrfast.gridy = 0;
		group.add(rdbtnFastradio);
		rdbtnCompleteRadio = new JRadioButton("complete");
		rdbtnCompleteRadio.addItemListener(this);
		GridBagConstraints constrcomp = new GridBagConstraints();
		constrcomp.insets = new Insets(10, 50, 5, 5);
		constrcomp.gridx = 1;
		constrcomp.gridy = 0;
		group.add(rdbtnCompleteRadio);
		rdbtnTotalRadioButton = new JRadioButton("total");
		rdbtnTotalRadioButton.addItemListener(this);
		GridBagConstraints constrtot = new GridBagConstraints();
		constrtot.insets = new Insets(10, 50, 5, 10);
		constrtot.gridx = 3;
		constrtot.gridy = 0;
		group.add(rdbtnTotalRadioButton);
		panel.add(rdbtnFastradio, constrfast);
		panel.add(rdbtnTotalRadioButton, constrtot);
		panel.add(rdbtnCompleteRadio, constrcomp);
		setJMenuBar(mb);
		this.getContentPane().add(panel);
		strBtn = new JButton("Start");
		strBtn.setEnabled(false);
		GridBagConstraints constrb1 = new GridBagConstraints();
		constrb1.insets = new Insets(0, 0, 10, 5);
		constrb1.gridx = 1;
		constrb1.gridy = 6;
		panel.add(strBtn, constrb1);
		stopBtn = new JButton("Stop");
		stopBtn.setEnabled(false);
		strBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tt = 120;
				if (rdbtnCompleteRadio.isSelected() || rdbtnFastradio.isSelected()
						|| rdbtnTotalRadioButton.isSelected()) {
					if (!rdbtnFastradio.isSelected())
						tt = 60 * 5;
					strBtn.setEnabled(false);

					stopBtn.setEnabled(true);
					try {
						ctr.saveConfiguration();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					int c = 0;
					ctr.sendCommand(ctr.getPassword(), GuiController.CMD_START);
					final int time = tt;
					bar.setMaximum(tt);
					Timer t = new Timer(1000, new ActionListener() {
						int v = 1;

						public void actionPerformed(ActionEvent e) {
							v++;
							increaseBar(v);
							if (v >= time) {
								strBtn.setEnabled(true);
								ctr.closeGUI();
							}
						}
					});
					t.start();
				} else {
					JOptionPane.showMessageDialog(statusPanel, "Seleziona un azione prima di premere start",
							"Action Error", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		stopBtn.setEnabled(false);
		stopBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				strBtn.setEnabled(true);
				stopBtn.setEnabled(false);
				ctr.sendStop();

			}
		});
		
		GridBagConstraints constrb2 = new GridBagConstraints();

		constrb2.insets = new Insets(0, 0, 10, 5);
		constrb2.gridx = 3;
		constrb2.gridy = 6;
		panel.add(stopBtn, constrb2);

	}
	public void disableStart() {
		this.strBtn.setEnabled(false);


	}
	
	public JProgressBar createProgress(int max) {
		JProgressBar bar = new JProgressBar(0, max);
		bar.setValue(0);
		bar.setStringPainted(true);
		return bar;
	}

	public void enableStartandStop() {
		this.strBtn.setEnabled(true);
		this.stopBtn.setEnabled(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.exit)) {
			this.dispose();
			ctr.sendClose(ctr.getPassword());
		} else if (e.getSource().equals(this.con)) {
			nodeD = new NodeDialog(this, "", true);
			nodeD.setVisible(true);
			// d.setModal(true);
		} else if (e.getSource().equals(this.info)) {
			String msg = "ZAK Control Panel by SSDDM\ninfo at www.ssddm.co.uk ";
			JOptionPane.showMessageDialog(this, msg, "Info", JOptionPane.INFORMATION_MESSAGE);
		} else if (e.getSource().equals(this.acq)) {
			AcqDialog d = new AcqDialog(this, "", true);
			d.setVisible(true);
			// d.setModal(true);
		} else if (e.getSource() == strBtn) {
			strBtn.setEnabled(false);
			stopBtn.setEnabled(true);
		} else if (e.getSource() == stopBtn) {
			ctr.close();
		} else if (e.getSource() == rdbtnCompleteRadio || e.getSource() == rdbtnFastradio
				|| e.getSource() == rdbtnTotalRadioButton) {
			if (strBtn.isEnabled())
				strBtn.setEnabled(false);

		}
	}

	public void itemStateChanged(ItemEvent e) {
		if(oldSource!=null &&oldSource.equals(e.getSource())){
			return;
		}else {
			oldSource=e.getSource();
		}
		rdbtnCompleteRadio.removeItemListener(this);
		rdbtnFastradio.removeItemListener(this);
		rdbtnTotalRadioButton.removeItemListener(this);
		
		if (e.getSource() instanceof JRadioButton) {
			
			JRadioButton btn= (JRadioButton)e.getSource();
			System.out.println(btn.getText());
			GuiController ctr = GuiController.getInstance(this);
			ctr.setComplete(btn.getText().equals("complete"));
			ctr.setFast(btn.getText().equals(this.rdbtnFastradio.getText()));
			ctr.setTotal(btn.getText().equals(this.rdbtnTotalRadioButton.getText()));
		}
		rdbtnCompleteRadio.addItemListener(this);
		rdbtnFastradio.addItemListener(this);
		rdbtnTotalRadioButton.addItemListener(this);
	}
	public void closeNodeD() {
		if(nodeD.isActive())
		this.nodeD.dispose();
	}

	public void stop() {
		ZeroMQ.instance().sendStop();
	}

	public void connected(ConnectionEvent event) {
		updStatusPanelToOK();
		enableStartandStop();
		closeNodeD();
	}

	public void disconnected(ConnectionEvent event) {
		updStatusPanelToStop();

	}

	public void setFlags(boolean fast, boolean comp, boolean tot) {

		GuiController ctr = GuiController.getInstance(this);
		rdbtnCompleteRadio.setSelected(comp);
		this.rdbtnFastradio.setSelected(fast);
		rdbtnTotalRadioButton.setSelected(tot);

	}

	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void windowClosing(WindowEvent e) {
		ctr.sendClose(ctr.getPassword());
	}

	public void windowClosed(WindowEvent e) {

	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {

	}

	public void windowActivated(WindowEvent e) {

	}

	public void windowDeactivated(WindowEvent e) {

	}
}
