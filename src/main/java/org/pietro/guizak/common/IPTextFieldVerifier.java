package org.pietro.guizak.common;

import java.awt.Frame;
import java.util.StringTokenizer;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;

public class IPTextFieldVerifier extends InputVerifier {
	public IPTextFieldVerifier() {
		super();
	}
	@Override
	public boolean verify(JComponent input) {
              String txt=((JTextField)input).getText();
               if (!txt.isEmpty()){
            	   	int value = Integer.parseInt(((JTextField) input).getText());
					if (value < 0 || value > 255) {
						// to prevent recursive calling of the
						// InputVerifier, set it to null and
						// restore it after the JOptionPane has
						// been clicked.
						JOptionPane.showMessageDialog(input, "Invalid value!", "Error",
								JOptionPane.ERROR_MESSAGE);
						// input.setInputVerifier(this);
						return false;
					}
				
				return true;
               }
	return true;
}
}
