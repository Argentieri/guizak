package org.pietro.guizak.common;

import java.util.EventListener;

public interface ConnectionListener extends EventListener{
	public void connected(ConnectionEvent event);
	public void disconnected(ConnectionEvent event);
}
