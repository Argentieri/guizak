package org.pietro.guizak.common;

import java.awt.Frame;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class PwdVerifier extends InputVerifier {

	@Override
	public boolean verify(JComponent input) {
			JPasswordField tField = (JPasswordField) input;
		    if (tField.getPassword().length == 8) {
		    	return true;
			}else{
				JOptionPane.showMessageDialog(input.getParent(), "password length should be 8 characters", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		  }
	}


