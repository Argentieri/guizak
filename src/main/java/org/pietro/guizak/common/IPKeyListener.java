package org.pietro.guizak.common;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class IPKeyListener implements KeyListener {

	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent e) {
	 
		
	}

	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			JTextField txtF=(JTextField)e.getSource();
			String txt=txtF.getText();
			 if (txt.trim().length()==0){	
				 Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
				    Container root = c.getFocusCycleRootAncestor();

				    FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
				     Component prevFocus = (Component) policy.getComponentBefore(root, c);
	    			 prevFocus.requestFocusInWindow();
				    
			 }
		 }else {
			 String txt=((JTextField)e.getSource()).getText();
	
			 if (txt.length()>=3){
			
			KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		    manager.focusNextComponent();
		}}
		
	}

}
