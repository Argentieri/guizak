package org.pietro.guizak.common;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class NumberFieldVerifier  extends InputVerifier{

	@Override
	public boolean verify(JComponent input) {
		try {
		      Integer.parseInt(((JTextField)input).getText());
		      return true;
		    } catch (NumberFormatException e) {
		     JOptionPane.showMessageDialog(input.getParent(), "PortValue is not valid", "Error", JOptionPane.WARNING_MESSAGE);
		     return false;
		    }
	}

}
