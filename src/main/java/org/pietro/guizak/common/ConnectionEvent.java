package org.pietro.guizak.common;

import java.util.EventObject;

public class ConnectionEvent extends EventObject {
	private boolean con;
	public ConnectionEvent(Object source,boolean connected){
		 super(source);
		 this.con=connected;
	}
	
}
