package org.zmq;

import org.zeromq.ZMsg;

import java.net.ConnectException;

import org.pietro.guizak.guizak.GuiController;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;
import org.slf4j.Logger;

public class ZeroMQ {
	private ZMQ.Socket socket=null;
	private ZMQ.Context context=null;
	private static ZeroMQ zeromq=null;
	private boolean connected=false;
	private static boolean error=false;
	GuiController ctr;
	//private int count =0;
    private static final Logger  logger = LoggerFactory.getLogger(ZeroMQ.class);

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	private ZeroMQ(){
		context = ZMQ.context(1);
		 this.socket = context.socket(ZMQ.REQ);
	     boolean ok=socket.connect ("tcp://localhost:5555");
	     ctr=GuiController.getInstance(null);
	     
	}
	
	public static ZeroMQ instance() {
		if (zeromq==null){
			zeromq=new ZeroMQ();
		}
		return zeromq;
	}
	
	
    /**
     * @param args
     * @throws ConnectException 
     */
	    public  boolean connect(String password,String ipAdd) throws ConnectException {
	        context = ZMQ.context(1);
	        StringBuilder msg=new StringBuilder(ipAdd);
	        msg=msg.append(password).append(ctr.iPClient).append(GuiController.CMD_CONNECT).append("******");
	        
	        ZMsg zmsg=new ZMsg();
	        
	        String m=msg.toString().replaceAll("\\.", "");
	 //       zmsg.add(m);
	        this.socket = context.socket(ZMQ.REQ);
	        String url="tcp://"+"localhost:"+ctr.getPort();
	        ctr.setUrl(url);
	        logger.debug("URL:"+ctr.getUrl());
	        boolean ok=socket.connect (ctr.getUrl());
	      //  count++;
	        logger.debug("Sending msg:\n"+m);
	        socket.send(m);
	        //System.out.println("COUNT:"+count);
	        String reply = new String(socket.recv());//Msg(socket);
	        logger.debug("Reiceved command:\n"+reply);
	        ok=reply.contains("Connection accepted")||!reply.contains("rejected")&&(!reply.contains("failed"));
	        if (reply.contains("FAST")){
	        	 ctr.setFast(true);
	        }else if (reply.contains("COMP")){
		         ctr.setComplete(true);
	        }else if (reply.contains("TOT")){
		         ctr.setTotal(true);
		         
	        }
	        if (!ok&&!error) {
	        	connected=false;
	        	 error=true;
	        	 throw new ConnectException(reply); 
	        
	        } 
	        return connected;
	    }
	    
	    public void closeConnection() {
		    if (socket!=null) {
		    	socket.close();
		    	context.close();
		    }
		    
	    }
	    
	    public String sendCmd(String cmd){
	       StringBuilder msg=new StringBuilder();
	   //    String m=msg.append(ctr.getIp()).append(ctr.getPassword()).append("198161017003").toString();
	       logger.debug("Sending msg:\n"+cmd);
	    	socket.send(cmd);
	    	logger.debug(("Length:"+cmd.length()));
	    	String resp=new String(socket.recv(0));
	    	logger.debug("Reiceved command:\n"+resp);
	    	System.out.println(resp);
	    	return resp;
	    }
	    public void sendStop() {
	    	StringBuilder msg=new StringBuilder();
	    	String m=msg.append(ctr.getIpServer()).append(ctr.getPassword()).append(ctr.getiPClient()).append(GuiController.CMD_STOP).toString();
	    	sendCmd(m);
		    logger.debug("Sending msg:\n"+m);
	    	String resp=new String(socket.recv(0));
	    	logger.debug("Reiceved command:\n"+resp);

	    }
   
}
    

