package org.zmq;

import org.pietro.guizak.guizak.GuiController;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;

public class Test {
	private ZMQ.Socket socket=null;

	public void sendConnection() {
		 Context context = ZMQ.context(1);
		 this.socket = context.socket(ZMQ.REQ);
	     boolean ok=socket.connect ("tcp://*:5555");
	     System.out.println("Connection:"+ok);
	     socket.send("192168001003Xy82Nd1C192168001002CONNECT******");
         
	    String resp=new String(socket.recv(0));
   	 	System.out.println(resp);
	    if (resp.startsWith("Connection accepted!")){
			String msg="192168001003Xy82Nd1C192168001002*******FAST*S";
			socket.send(msg);
	    	String resp2=new String(socket.recv(0));
	    	System.out.println(resp2);
	    	msg="192168001003Xy82Nd1C192168001002************P";
			socket.send(msg);
	    	String resp3=new String(socket.recv(0));
	    	System.out.println(resp3);
	        msg="192168001003Xy82Nd1C192168001002*******COMP*S";
			socket.send(msg);
	    	String resp4=new String(socket.recv(0));
	    	System.out.println(resp4);
	    }
	   // String resp_=new String(socket.recv(0));
	   // System.out.println(resp_);
	} 
	public static void main(String[] args) {
     new Test().sendConnection();
     System.exit(0);
	}

}
